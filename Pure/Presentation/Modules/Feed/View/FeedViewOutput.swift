//
//  FeedFeedViewOutput.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//

protocol FeedViewOutput {
    var items: [FeedContentItemProtocol] { get }
    
    func viewIsReady()
    func willDisplayLastCell()
    func handleItemSelect(_ item: FeedContentItemProtocol)
}
