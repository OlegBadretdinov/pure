//
//  FeedFeedInitializer.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//

import UIKit

class FeedModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var feedViewController: FeedViewController!

    override func awakeFromNib() {

        let configurator = FeedModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: feedViewController)
    }

}
