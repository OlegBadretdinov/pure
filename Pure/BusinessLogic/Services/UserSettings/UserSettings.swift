//
//  UserSettings.swift
//  Pure
//
//  Created by Oleg Badretdinov on 05.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit

protocol UserSettingsProtocol {
    var  token: String? { get set }
}

class UserSettings: UserSettingsProtocol {
    var token: String? {
        get {
            return UserDefaults.standard.string(forKey: .token)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: .token)
        }
    }
}
