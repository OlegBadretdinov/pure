//
//  FeedAssembly.swift
//  Pure
//
//  Created by Oleg Badretdinov on 06.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import Swinject
import Moya
import TwitterKit

class FeedAssembly: Assembly {
    func assemble(container: Container) {
        container.register(MoyaProvider<GithubFeedNetworkService>.self) { _ in
            return MoyaProvider<GithubFeedNetworkService>()
        }.inObjectScope(.transient)
        container.register(TWTRAPIClient.self) { _ in
            TWTRAPIClient.withCurrentUser()
        }
        container.register(TWTRUserTimelineDataSource.self) { (resolver) -> TWTRUserTimelineDataSource in
            let client = resolver.resolve(TWTRAPIClient.self)!
            return TWTRUserTimelineDataSource(screenName: "kojima_hideo", apiClient: client) // replace userid with client.userID
        }
        
        container.register([FeedContentServiceProtocol].self) { (resolver) -> [FeedContentServiceProtocol] in
            let githubNetwork = resolver.resolve(MoyaProvider<GithubFeedNetworkService>.self)!
            let twitterDataSource = resolver.resolve(TWTRUserTimelineDataSource.self)!
            return [TwitterContentService(dataSource: twitterDataSource), GithubContentService(network: githubNetwork)]
        }.inObjectScope(.transient)
    }
}
