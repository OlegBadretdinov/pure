//
//  TwitterContentService.swift
//  Pure
//
//  Created by Oleg Badretdinov on 06.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import TwitterKit
import RxSwift

class TwitterContentService: NSObject, FeedContentServiceProtocol {
    
    fileprivate let dataSource: TWTRUserTimelineDataSource
    fileprivate var position: String!
    fileprivate let subject = PublishSubject<[FeedContentItemProtocol]>()
    
    var inProgress: Bool = false
    var items: [FeedContentItemProtocol] = []
    
    var itemsObservable: Observable<[FeedContentItemProtocol]> {
        return self.subject.asObservable()
    }
    
    init(dataSource: TWTRUserTimelineDataSource) {
        self.dataSource = dataSource
    }
    
    func fetchContent() {
        if !self.inProgress {
            self.inProgress = true
            self.dataSource.loadPreviousTweets(beforePosition: self.position) { [weak self] (tweets, cursor, error) in
                guard let `self` = self else { return }
                self.inProgress = false
                
                if let tweets = tweets, let cursor = cursor {
                    self.handleTweets(tweets)
                    self.position = cursor.minPosition
                } else if let error = error {
                    self.subject.onError(error)
                }
            }
        }
    }
}

extension TwitterContentService {
    fileprivate func handleTweets(_ tweets: [TWTRTweet]) {
        var newItems: [FeedContentItemProtocol] = []
        
        for tweet in tweets {
            let item = TwitterFeedEntity(title: tweet.author.name, text: tweet.text, date: tweet.createdAt, url: tweet.permalink)
            newItems.append(item)
        }
        self.items.append(contentsOf: newItems)
        
        self.subject.onNext(newItems)
    }
}
