//
//  AuthorizationAuthorizationInteractor.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//
import Foundation

class AuthorizationInteractor: AuthorizationInteractorInput {

    weak var output: AuthorizationInteractorOutput!
    var authService: AuthorizationServiceProtocol!
    var userSettings: UserSettingsProtocol!
    
    func signIn() {
        self.authService.startAuth()
    }
    
    func handleWebViewUrl(_ url: URL) {
        self.authService.handleURL(url)
    }
}

extension AuthorizationInteractor: AuthorizationServiceDelegate {
    func handleError(_ error: Error) {
        self.output.handleError(error)
    }
    
    func shouldGoToURL(_ url: URL) {
        let request = URLRequest(url: url)
        self.output.shouldPerformRequest(request)
    }
    
    func didReceivedToken(_ token: String) {
        self.userSettings.token = token
        self.output.loginDidFinish()
    }
}
