//
//  AuthorizationAuthorizationInteractorInput.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//

import Foundation

protocol AuthorizationInteractorInput {
    func signIn()
    func handleWebViewUrl(_ url: URL)
}
