//
//  UserDefaultsExtensions.swift
//  Pure
//
//  Created by Oleg Badretdinov on 05.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import Foundation

enum UserDefaultsKeys: String {
    case token = ""
}

extension UserDefaults {
    func set<T>(_ value: T, forKey key: UserDefaultsKeys) {
        self.set(value, forKey: key.rawValue)
    }
    
    func removeObject(forKey key: UserDefaultsKeys) {
        self.removeObject(forKey: key.rawValue)
    }
    
    func bool(forKey key: UserDefaultsKeys) -> Bool {
        return self.bool(forKey: key.rawValue)
    }
    
    func double(forKey key: UserDefaultsKeys) -> Double {
        return self.double(forKey: key.rawValue)
    }
    
    func integer(forKey key: UserDefaultsKeys) -> Int {
        return self.integer(forKey: key.rawValue)
    }
    
    func string(forKey key: UserDefaultsKeys) -> String? {
        return self.string(forKey: key.rawValue)
    }
    
    func object(forKey key: UserDefaultsKeys) -> Any? {
        return self.object(forKey: key.rawValue)
    }
}
