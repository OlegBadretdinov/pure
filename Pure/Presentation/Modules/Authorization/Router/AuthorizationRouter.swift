//
//  AuthorizationAuthorizationRouter.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//
import LightRoute

class AuthorizationRouter: AuthorizationRouterInput {
    private let feedSegueIdentifier = "FeedSegue"
    
    weak var transitionHandler: TransitionHandler!
    
    func showFeedScreen() {
        try! self.transitionHandler.forSegue(identifier: self.feedSegueIdentifier, to: FeedModuleInput.self).perform()
    }
}
