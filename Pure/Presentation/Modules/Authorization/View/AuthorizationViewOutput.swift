//
//  AuthorizationAuthorizationViewOutput.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//
import Foundation

protocol AuthorizationViewOutput {
    func didTapLoginButton()
    func didNavigateToUrl(_ url: URL)
}
