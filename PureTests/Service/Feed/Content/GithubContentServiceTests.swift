//
//  GithubContentServiceTests.swift
//  PureTests
//
//  Created by Oleg Badretdinov on 11.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import XCTest
import Moya
import RxSwift
@testable import Pure

class GithubContentServiceTests: XCTestCase {
    var service: GithubContentService!
    var provider: MoyaProvider<GithubFeedNetworkService>!
    var disposeBag: DisposeBag!
    
    override func setUp() {
        self.disposeBag = DisposeBag()
    }
    
    override func tearDown() {
        self.service = nil
        self.provider = nil
        self.disposeBag = nil
    }
    
    func successResponseStub() {
        let endpointClosure = { (target: GithubFeedNetworkService) -> Endpoint in
            return Endpoint(url: URL(target: target).absoluteString, sampleResponseClosure: { () -> EndpointSampleResponse in
                let bundle = Bundle(for: type(of: self))
                let data = try! Data(contentsOf: bundle.url(forResource: "GithubContent", withExtension: "json")!)
                return EndpointSampleResponse.networkResponse(200, data)
            }, method: target.method, task: target.task, httpHeaderFields: target.headers)
        }
        self.provider = MoyaProvider<GithubFeedNetworkService>(endpointClosure: endpointClosure, stubClosure: MoyaProvider.immediatelyStub)
        self.service = GithubContentService(network: self.provider)
    }
    
    func errorResponseStub() {
        let endpointClosure = { (target: GithubFeedNetworkService) -> Endpoint in
            return Endpoint(url: URL(target: target).absoluteString, sampleResponseClosure: { () -> EndpointSampleResponse in
                return EndpointSampleResponse.networkError(NSError(domain: "Test", code: 500, userInfo: nil))
            }, method: target.method, task: target.task, httpHeaderFields: target.headers)
        }
        self.provider = MoyaProvider<GithubFeedNetworkService>(endpointClosure: endpointClosure, stubClosure: MoyaProvider.immediatelyStub)
        self.service = GithubContentService(network: self.provider)
    }
    
    func testObjectSerializing() {
        self.successResponseStub()
        
        self.service.fetchContent()
        XCTAssert(self.service.items.count == 2)
        
        let first = self.service.items.first!
        
        XCTAssert(first.text == "Reactive Programming in Swift")
        XCTAssert(first.title == "ReactiveX/RxSwift")
        XCTAssert(first.url == URL(string: "https://api.github.com/repos/ReactiveX/RxSwift"))
        XCTAssert(first.date == DateFormatter.iso8601Full.date(from: "2015-04-07T21:25:17Z"))
    }
    
    func testSecondFetch() {
        self.successResponseStub()
        
        self.service.fetchContent()
        self.service.fetchContent()
        
        XCTAssert(self.service.items.count == 4)
    }
    
    func testSuccessSubscription() {
        self.successResponseStub()
        
        let expectation = XCTestExpectation(description: "On next expectation")
        
        self.service.itemsObservable.subscribe(onNext: { (items) in
            XCTAssert(items.count == 2)
            expectation.fulfill()
        }).disposed(by: self.disposeBag)
        self.service.fetchContent()
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testErrorSubscription() {
        self.errorResponseStub()
        
        let expectation = XCTestExpectation(description: "On next expectation")
        
        self.service.itemsObservable.subscribe(onNext: nil, onError: { (error) in
            expectation.fulfill()
        }).disposed(by: self.disposeBag)
        self.service.fetchContent()
        
        wait(for: [expectation], timeout: 1.0)
    }
}
