//
//  AuthorizationAuthorizationInitializer.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//

import UIKit

class AuthorizationModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var authorizationViewController: AuthorizationViewController!

    override func awakeFromNib() {

        let configurator = AuthorizationModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: authorizationViewController)
    }

}
