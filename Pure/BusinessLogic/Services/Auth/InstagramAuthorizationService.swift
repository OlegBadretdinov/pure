//
//  InstagramAuthorizationService.swift
//  Pure
//
//  Created by Oleg Badretdinov on 06.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit

class InstagramAuthorizationService: AuthorizationServiceProtocol {
    fileprivate static let clientId = "2541e06c8a3c4d55aeae721221aab426"
    fileprivate static let clientSecret = "c90903cac023479daca124cdf8142dcd"
    fileprivate static let redirectUrl = "https://example.com"
    fileprivate static let tokenKey = "access_token"
    fileprivate static let scopes: [String] = ["basic"]
    
    fileprivate var url: URL? {
        return URL(string: String(format: "https://api.instagram.com/oauth/authorize/?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True",
                                  InstagramAuthorizationService.clientId,
                                  InstagramAuthorizationService.redirectUrl,
                                  InstagramAuthorizationService.scopes.joined(separator: "+")))
    }
    
    weak var delegate: AuthorizationServiceDelegate?
    
    func startAuth() {
        if let url = self.url {
            self.delegate?.shouldGoToURL(url)
        }
    }
    
    func handleURL(_ url: URL) {
        if let components = URLComponents(url: url, resolvingAgainstBaseURL: true), let host = components.host, let fragment = components.fragment, InstagramAuthorizationService.redirectUrl.contains(host) {
            let items = fragment.components(separatedBy: "=")
            if items.count == 2, items.first == InstagramAuthorizationService.tokenKey, let token = items.last {
                self.delegate?.didReceivedToken(token)
            }
        }
    }
}
