//
//  UIFontExtension.swift
//  Pure
//
//  Created by Oleg Badretdinov on 11.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit

extension UIFont {
    enum FeedCell {
        static let title = UIFont.systemFont(ofSize: 20)
        static let text = UIFont.systemFont(ofSize: 17)
        static let date = UIFont.systemFont(ofSize: 14)
    }
}
