//
//  FeedFeedViewController.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController, FeedViewInput {
    fileprivate static let CellIdentifier = "TitleCell"
    @IBOutlet weak var tableView: UITableView!
    
    var output: FeedViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.output.viewIsReady()
    }
    
    fileprivate func setupTableView() {
        self.tableView.register(FeedItemTableViewCell.self, forCellReuseIdentifier: FeedViewController.CellIdentifier)
    }

    // MARK: FeedViewInput
    func setupInitialState() {
        self.tableView.isHidden = true
    }
    
    func addRows(count: Int) {
        if self.tableView.isHidden {
            self.tableView.isHidden = false
        }
        
        let totalRows = self.output.items.count
        let paths = Array((totalRows - count)..<totalRows).map({ return IndexPath(row: $0, section: 0) })
        self.tableView.insertRows(at: paths, with: .automatic)
    }
    
    func showError(_ error: Error) {
        let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension FeedViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.output.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedViewController.CellIdentifier, for: indexPath) as! FeedItemTableViewCell
        let item = self.output.items[indexPath.row]
        cell.dateTextLabel.text = DateFormatter.defaultFormatter.string(from: item.date)
        cell.titleLabel.text = item.title
        cell.textLabel?.text = item.text
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = self.output.items[indexPath.row]
        let height = FeedItemTableViewCell.heightForCell(withText: item.text, width: tableView.frame.size.width)
        return height
    }
}

extension FeedViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row >= self.output.items.count - 1 {
            self.output.willDisplayLastCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.output.items[indexPath.row]
        self.output.handleItemSelect(item)
    }
}
