//
//  FeedFeedInteractor.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//
import RxSwift

class FeedInteractor: FeedInteractorInput {
    fileprivate let disposeBag = DisposeBag()
    
    weak var output: FeedInteractorOutput!
    var contentServices: [FeedContentServiceProtocol]!
    var items: [FeedContentItemProtocol] = []
    
    func setupInteractor() {
        self.items = self.contentServices.flatMap({ return $0.items })
        self.setupObservables()
    }

    func fetchContent() {
        for contentService in self.contentServices {
            contentService.fetchContent()
        }
    }
}

extension FeedInteractor {
    fileprivate func setupObservables() {
        let observables = self.contentServices.map({ $0.itemsObservable })
        let resultObs = Observable.merge(observables)
        resultObs.subscribe(onNext: { [weak self] (items) in
            guard let `self` = self else { return }
            self.items.append(contentsOf: items)
            self.output.didReceiveNewItems(items)
            }, onError: { [weak self] (error) in
                guard let `self` = self else { return }
                self.output.didReceiveError(error)
            }, onCompleted: nil, onDisposed: { [weak self] in
                guard let `self` = self else { return }
                self.setupObservables()
        }).disposed(by: self.disposeBag)
    }
}
