//
//  AuthorizationAuthorizationViewController.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//

import UIKit
import WebKit

class AuthorizationViewController: UIViewController, AuthorizationViewInput {
    fileprivate var webViewShown = false
    var output: AuthorizationViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: IBOutlet
    @IBAction func handleLoginButton(_ sender: Any) {
        self.output.didTapLoginButton()
    }
    

    // MARK: AuthorizationViewInput
    
    func presentWebView(withUrlRequest urlRequest: URLRequest) {
        let webView = WKWebView()
        webView.navigationDelegate = self
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.load(urlRequest)
        
        let ctrl = self.getController(forWebView: webView)
        
        self.webViewShown = true
        self.present(ctrl, animated: true, completion: nil)
    }
    
    func closeWebViewIfNeeded(completion: (() -> Void)?) {
        if self.webViewShown {
            self.closeWebViewWithCompletion(completion)
        } else {
            completion?()
        }
    }
    
    func showErrorAlert(_ error: Error) {
        let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension AuthorizationViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url {
            self.output.didNavigateToUrl(url)
        }
        decisionHandler(.allow)
    }
}

extension AuthorizationViewController {
    fileprivate func getController(forWebView webView: WKWebView) -> UIViewController {
        let ctrl = UIViewController()
        ctrl.view.backgroundColor = .white
        ctrl.view.addSubview(webView)
        
        webView.frame = ctrl.view.bounds
        
        let nav = UINavigationController(rootViewController: ctrl)
        nav.navigationBar.isTranslucent = false
        
        ctrl.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.closeWebView))
        
        return nav
    }
    
    @objc fileprivate func closeWebView() {
        self.closeWebViewIfNeeded(completion: nil)
    }
    
    fileprivate func closeWebViewWithCompletion(_ completion: (()->Void)?) {
        self.webViewShown = false
        self.dismiss(animated: true, completion: completion)
    }
}
