//
//  FeedContentServiceProtocol.swift
//  Pure
//
//  Created by Oleg Badretdinov on 06.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit
import RxSwift

protocol FeedContentItemProtocol {
    var title: String { get }
    var text: String { get }
    var date: Date { get }
    var url: URL { get }
}

protocol FeedContentServiceProtocol: class {
    var itemsObservable: Observable<[FeedContentItemProtocol]> { get }
    var items: [FeedContentItemProtocol] { get }
    var inProgress: Bool { get }
    
    func fetchContent()
}
