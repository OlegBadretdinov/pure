//
//  AuthorizationAuthorizationPresenter.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//
import Foundation

class AuthorizationPresenter: AuthorizationModuleInput, AuthorizationViewOutput, AuthorizationInteractorOutput {
    weak var view: AuthorizationViewInput!
    var interactor: AuthorizationInteractorInput!
    var router: AuthorizationRouterInput!

    func viewIsReady() {
        
    }
    
    func didTapLoginButton() {
        self.interactor.signIn()
    }
    
    func loginDidFinish() {
        self.view.closeWebViewIfNeeded { [weak self] in
            guard let `self` = self else { return }
            self.router.showFeedScreen()
        }
    }
    
    func shouldPerformRequest(_ request: URLRequest) {
        self.view.presentWebView(withUrlRequest: request)
    }
    
    func didNavigateToUrl(_ url: URL) {
        self.interactor.handleWebViewUrl(url)
    }
    
    func handleError(_ error: Error) {
        self.view.showErrorAlert(error)
    }
}
