//
//  GithubResponseObject.swift
//  Pure
//
//  Created by Oleg Badretdinov on 08.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit

class GithubResponseObject: NSObject, Decodable {
    var total_count: Int = 0
    var items: [GithubFeedEntity] = []
}
