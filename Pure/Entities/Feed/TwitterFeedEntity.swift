//
//  TwitterFeedEntity.swift
//  Pure
//
//  Created by Oleg Badretdinov on 07.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit

class TwitterFeedEntity: NSObject, FeedContentItemProtocol {
    var title: String
    var text: String
    var date: Date
    var url: URL
    
    init(title: String, text: String, date: Date, url: URL) {
        self.title = title
        self.text = text
        self.date = date
        self.url = url
    }
}
