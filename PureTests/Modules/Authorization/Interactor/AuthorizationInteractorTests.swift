//
//  AuthorizationAuthorizationInteractorTests.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//

import XCTest
@testable import Pure

class AuthorizationInteractorTests: XCTestCase {
    var interactor: AuthorizationInteractor!
    var presenter: MockPresenter!
    var service: MockService!
    
    override func setUp() {
        super.setUp()
        self.interactor = AuthorizationInteractor()
        self.presenter = MockPresenter()
        self.service = MockService()
        self.interactor.output = self.presenter
        self.interactor.authService = self.service
    }

    override func tearDown() {
        super.tearDown()
        self.interactor = nil
        self.presenter = nil
        self.service = nil
    }
    
    class MockService: AuthorizationServiceProtocol {
        var delegate: AuthorizationServiceDelegate?
        
        var didStartAuth = false
        var lastURL: URL!
        
        func startAuth() {
            self.didStartAuth = true
        }
        
        func handleURL(_ url: URL) {
            self.lastURL = url
        }
    }

    class MockPresenter: AuthorizationInteractorOutput {
        var lastRequest: URLRequest!
        
        func loginDidFinish() {
            
        }
        
        func shouldPerformRequest(_ request: URLRequest) {
            self.lastRequest = request
        }
        
        func handleError(_ error: Error) {
            
        }
    }
    
    func testAuth() {
        self.interactor.signIn()
        XCTAssert(self.service.didStartAuth)
    }
    
    func testURLRequest() {
        let url = URL(string: "https://example.com")!
        self.interactor.shouldGoToURL(url)
        XCTAssert(self.presenter.lastRequest.url == url)
    }
    
    func testUrlHandling() {
        let url = URL(string: "https://example.com")!
        self.interactor.handleWebViewUrl(url)
        XCTAssert(self.service.lastURL == url)
    }
}
