//
//  AppAssembly.swift
//  Pure
//
//  Created by Oleg Badretdinov on 02.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit
import Swinject

class AppAssembly: NSObject {
    static var assembler: Assembler = {
        return Assembler([AuthAssembly(), FeedAssembly()])
    }()
}
