//
//  TwitterAuthorizationService.swift
//  Pure
//
//  Created by Oleg Badretdinov on 06.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit
import TwitterKit

class TwitterAuthorizationService: NSObject, AuthorizationServiceProtocol {
    var delegate: AuthorizationServiceDelegate?
    
    func startAuth() {
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            if let session = session {
                self.delegate?.didReceivedToken(session.authToken)
            } else if let error = error {
                self.delegate?.handleError(error)
            }
        })
    }
    
    func handleURL(_ url: URL) {
        
    }
    
    override init() {
        super.init()
        TWTRTwitter.sharedInstance().start(withConsumerKey:"cbOvHwRzpaHT5W00z0yrjyF6v", consumerSecret:"aKHypCoVXN1m58W6P7QIL25jlLoqpDLIc2wmyPf4C0fJ4ibsOP")
    }
}
