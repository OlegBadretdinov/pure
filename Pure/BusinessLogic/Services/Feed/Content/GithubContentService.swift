//
//  GithubContentService.swift
//  Pure
//
//  Created by Oleg Badretdinov on 06.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit
import Moya
import RxMoya
import RxSwift

class GithubContentService: NSObject, FeedContentServiceProtocol {
    fileprivate var disposeBag = DisposeBag()
    fileprivate var currentPage = 0
    fileprivate let network: MoyaProvider<GithubFeedNetworkService>
    fileprivate let subject = PublishSubject<[FeedContentItemProtocol]>()
    var itemsObservable: Observable<[FeedContentItemProtocol]> {
        return self.subject.asObservable()
    }
    var items: [FeedContentItemProtocol] = []
    var inProgress: Bool = false
    
    init(network: MoyaProvider<GithubFeedNetworkService>) {
        self.network = network
    }
    
    func fetchContent() {
        if !self.inProgress {
            self.inProgress = true
            self.network.rx.request(.searchRepositories(request: "swift", page: self.currentPage)).map(GithubResponseObject.self).subscribe(onSuccess: { [weak self] (response) in
                    guard let `self` = self else { return }
                    self.handleItems(response.items)
                    self.inProgress = false
                }, onError: { [weak self] (error) in
                    guard let `self` = self else { return }
                    self.subject.onError(error)
                    self.inProgress = false
            }).disposed(by: self.disposeBag)
        }
    }
}

extension GithubContentService {
    fileprivate func handleItems(_ items: [GithubFeedEntity]) {
        self.items.append(contentsOf: items)
        self.subject.onNext(items)
        self.currentPage += 1
    }
}
