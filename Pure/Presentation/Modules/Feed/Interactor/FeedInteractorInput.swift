//
//  FeedFeedInteractorInput.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//

import Foundation

protocol FeedInteractorInput {
    var items: [FeedContentItemProtocol] { get }
    func setupInteractor()
    func fetchContent()
}
