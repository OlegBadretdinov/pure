//
//  FeedItemTableViewCell.swift
//  Pure
//
//  Created by Oleg Badretdinov on 10.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit

class FeedItemTableViewCell: UITableViewCell {
    fileprivate static let defaultMargin: CGFloat = 8
    
    override var textLabel: UILabel? {
        return self.baseTextLabel
    }
    fileprivate let baseTextLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.FeedCell.text
        label.numberOfLines = 0
        return label
    }()
    let dateTextLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.FeedCell.date
        return label
    }()
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.FeedCell.title
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupSubviews()
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init(coder: aDecoder)
        self.setupSubviews()
    }
    
    fileprivate func setupSubviews() {
        self.addSubview(self.baseTextLabel)
        self.addSubview(self.dateTextLabel)
        self.addSubview(self.titleLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let margin = FeedItemTableViewCell.defaultMargin
        let preferedWidth = self.frame.size.width - margin * 2
        
        let titleHeight = self.titleLabel.font.lineHeight
        self.titleLabel.frame = CGRect(x: margin, y: margin, width: preferedWidth, height: titleHeight)
        
        let textHeight = (self.baseTextLabel.text ?? "").heightWithConstrainedWidth(preferedWidth, font: self.baseTextLabel.font)!
        self.baseTextLabel.frame = CGRect(x: margin, y: self.titleLabel.frame.maxY + margin, width: preferedWidth, height: textHeight)
        
        let dateHeight = self.dateTextLabel.font.lineHeight
        self.dateTextLabel.frame = CGRect(x: margin, y: self.baseTextLabel.frame.maxY + margin, width: preferedWidth, height: dateHeight)
    }
}

extension FeedItemTableViewCell {
    class func heightForCell(withText text: String, width: CGFloat) -> CGFloat {
        let margin = FeedItemTableViewCell.defaultMargin
        let preferedWidth = width - margin * 2
        
        let textHeight = text.heightWithConstrainedWidth(preferedWidth, font: UIFont.FeedCell.text)!
        
        return textHeight + UIFont.FeedCell.date.lineHeight + UIFont.FeedCell.title.lineHeight + 4 * margin
    }
}
