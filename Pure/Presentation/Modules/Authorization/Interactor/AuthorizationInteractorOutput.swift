//
//  AuthorizationAuthorizationInteractorOutput.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//

import Foundation

protocol AuthorizationInteractorOutput: class {
    func loginDidFinish()
    func shouldPerformRequest(_ request: URLRequest)
    func handleError(_ error: Error)
}
