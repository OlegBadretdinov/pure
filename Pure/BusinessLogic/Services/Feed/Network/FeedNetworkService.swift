//
//  FeedNetworkService.swift
//  Pure
//
//  Created by Oleg Badretdinov on 08.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import Moya

enum GithubFeedNetworkService {
    case searchRepositories(request: String, page: Int)
}

extension GithubFeedNetworkService: TargetType {
    var baseURL: URL {
        return URL(string: "https://api.github.com")!
    }
    
    var path: String {
        switch self {
        case .searchRepositories(request: _, page: _):
            return "search/repositories"
        }
    }
    
    var method: Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .searchRepositories(request: let requestString, page: let page):
            let params = ["q" : requestString, "page" : page] as [String : Any]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
}
