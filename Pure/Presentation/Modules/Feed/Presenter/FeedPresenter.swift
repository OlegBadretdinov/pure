//
//  FeedFeedPresenter.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//

class FeedPresenter: FeedModuleInput, FeedViewOutput {
    weak var view: FeedViewInput!
    var interactor: FeedInteractorInput!
    var router: FeedRouterInput!
    
    var items: [FeedContentItemProtocol] {
        return self.interactor.items
    }

    func viewIsReady() {
        self.view.setupInitialState()
        self.interactor.setupInteractor()
        if self.items.isEmpty {
            self.interactor.fetchContent()
        }
    }
    
    func willDisplayLastCell() {
        self.interactor.fetchContent()
    }
    
    func handleItemSelect(_ item: FeedContentItemProtocol) {
        self.router.openUrl(item.url)
    }
}

extension FeedPresenter: FeedInteractorOutput {
    func didReceiveNewItems(_ items: [FeedContentItemProtocol]) {
        self.view.addRows(count: items.count)
    }
    
    func didReceiveError(_ error: Error) {
        self.view.showError(error)
    }
}
