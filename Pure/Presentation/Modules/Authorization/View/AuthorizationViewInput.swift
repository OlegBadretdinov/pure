//
//  AuthorizationAuthorizationViewInput.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//
import Foundation

protocol AuthorizationViewInput: class {
    func showErrorAlert(_ error: Error)
    func presentWebView(withUrlRequest urlRequest: URLRequest)
    func closeWebViewIfNeeded(completion: (()->Void)?)
}
