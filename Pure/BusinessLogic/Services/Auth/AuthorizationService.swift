//
//  AuthorizationService.swift
//  Pure
//
//  Created by Oleg Badretdinov on 02.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit

protocol AuthorizationServiceProtocol {
    var delegate: AuthorizationServiceDelegate? { get set }
    func startAuth()
    func handleURL(_ url: URL)
}

protocol AuthorizationServiceDelegate: class {
    func shouldGoToURL(_ url: URL)
    func didReceivedToken(_ token: String)
    func handleError(_ error: Error)
}
