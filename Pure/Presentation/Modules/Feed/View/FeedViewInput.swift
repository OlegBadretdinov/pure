//
//  FeedFeedViewInput.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//

protocol FeedViewInput: class {
    func setupInitialState()
    func addRows(count: Int)
    func showError(_ error: Error)
}
