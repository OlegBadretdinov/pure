//
//  AuthAssembly.swift
//  Pure
//
//  Created by Oleg Badretdinov on 02.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit
import Swinject

class AuthAssembly: Assembly {
    func assemble(container: Container) {
        container.register(AuthorizationServiceProtocol.self) { _ in
            TwitterAuthorizationService()
        }.inObjectScope(.transient)
        
        container.register(UserSettingsProtocol.self) { _ in
            UserSettings()
        }.inObjectScope(.container)
    }
}
