//
//  GithubFeedEntity.swift
//  Pure
//
//  Created by Oleg Badretdinov on 09.08.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit

class GithubFeedEntity: NSObject, Decodable, FeedContentItemProtocol {
    var title: String
    var text: String
    var url: URL
    var date: Date
    
    enum CodingKeys: String, CodingKey {
        case title = "full_name"
        case text = "description"
        case url = "url"
        case date = "created_at"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.text = try container.decode(String.self, forKey: .text)
        self.title = try container.decode(String.self, forKey: .title)
        
        let urlString = try container.decode(String.self, forKey: .url)
        self.url = URL(string: urlString)!
        
        let dateStr = try container.decode(String.self, forKey: .date)
        self.date = DateFormatter.iso8601Full.date(from: dateStr)!
    }
}
