//
//  AuthorizationAuthorizationConfigurator.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//

import UIKit
import Swinject

class AuthorizationModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? AuthorizationViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: AuthorizationViewController) {
        let resolver = AppAssembly.assembler.resolver
        
        let router = AuthorizationRouter()

        let presenter = AuthorizationPresenter()
        presenter.view = viewController
        presenter.router = router
        
        router.transitionHandler = viewController
        
        let interactor = AuthorizationInteractor()
        interactor.output = presenter
        interactor.authService = resolver.resolve(AuthorizationServiceProtocol.self)
        interactor.authService?.delegate = interactor
        interactor.userSettings = resolver.resolve(UserSettingsProtocol.self)

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
