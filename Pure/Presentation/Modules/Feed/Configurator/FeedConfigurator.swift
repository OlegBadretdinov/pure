//
//  FeedFeedConfigurator.swift
//  Pure
//
//  Created by Oleg Badretdinov on 01/08/2018.
//  Copyright © 2018 Pure. All rights reserved.
//

import UIKit

class FeedModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? FeedViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: FeedViewController) {
        let resolver = AppAssembly.assembler.resolver
        
        let router = FeedRouter()

        let presenter = FeedPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = FeedInteractor()
        interactor.output = presenter
        interactor.contentServices = resolver.resolve([FeedContentServiceProtocol].self)

        presenter.interactor = interactor
        viewController.output = presenter
    }
}
